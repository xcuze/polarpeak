$:.unshift File.expand_path("./../../lib", __FILE__)

ENV['RACK_ENV'] = 'test'

require 'bundler'
Bundler.require
require 'minitest/autorun'
require 'polar_peak'

require './config/environments'

module WithRollback
  def temporarily(&block)
    ActiveRecord::Base.connection.transaction do
      block.call
      raise ActiveRecord::Rollback
    end
  end
end
