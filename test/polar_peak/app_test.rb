require './test/test_helper'
require 'rack/test'
require 'sinatra/base'
require './app'

class APPTest < MiniTest::Test
  include Rack::Test::Methods
  include WithRollback

  def app
    Sinatra::Application
  end

  def test_empty_peaks
    get '/peaks'
    assert_equal "[]", last_response.body
  end

  def test_create_peak
    temporarily do
      # create a new peak
      data = {:name => "Mont Blanc", :location => "Alps", :height => 4808}
      post '/peaks', data.to_json, "CONTENT_TYPE" => "application/json"
      assert_equal 201, last_response.status

      # query peaks
      get '/peaks'
      response = JSON.parse(last_response.body)
      assert_equal 1, response.length
      assert_equal data[:name], response.first["name"]
      assert_equal data[:location], response.first["location"]
      assert_equal data[:height], response.first["height"]
    end
  end
end
