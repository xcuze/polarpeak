require './test/test_helper'

class PeakTest < Minitest::Test
  include WithRollback

  def test_it_exists
    assert_equal 0, Peak.count
    temporarily do
      Peak.create(:name => "Mont Blanc", :location => "Alps", :height => 4808)
      assert_equal 1, Peak.count
    end
    assert_equal 0, Peak.count
  end

end
