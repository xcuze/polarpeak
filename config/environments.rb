
require 'bundler'
Bundler.require

require 'yaml'

environment = ENV.fetch('RACK_ENV') { 'development' }

case environment
when 'test'
  db_options = YAML.load(File.read('./config/database.yml'))
  ActiveRecord::Base.establish_connection(db_options['test'])
else
  set :database_file, "./config/database.yml"
end

require 'polar_peak'
