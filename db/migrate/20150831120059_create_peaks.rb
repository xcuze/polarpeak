class CreatePeaks < ActiveRecord::Migration
  def change
    create_table :peaks do |t|
      t.string  :name
      t.string  :location
      t.integer :height
    end
  end
end
