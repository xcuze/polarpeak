require 'json'
require 'sinatra'
require 'sinatra/activerecord'

require './config/environments'

get "/peaks/?" do
  Peak.all.to_json
end

post "/peaks/?" do
  pass unless request.accept? 'application/json'
  json = JSON.load request.body

  Peak.create(json)

  status 201
end
