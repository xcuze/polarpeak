# PolarPeak 5108

A simple project that demonstrates how to use Sinatra, ActiveRecord and Heroku together AND have a
local development environment using sqlite3 (for simplicity).

Gems used:

- sinatra
- activerecord
- sinatra-activerecord
- sqlite3
- pg

## Usage

Some tips on how to use it.

### Getting Set-up

First, you need to create and migrate the database:

```bash
bundle exec rake db:create:all
bundle exec rake db:migrate # for 'development'
RACK_ENV=test exec rake db:migrate # for 'test'
```

### Testing

```bash
bundle exec rake test
```

### Running locally (in development)

```bash
bundle exec rackup -p 9292
```
